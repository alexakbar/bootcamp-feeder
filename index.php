<?php
include_once('Connection/db.php'); 

// get data matkul yang akan di sync
function getDataMatkul()
{
    $schema = 'public';
    $tablename = 'matkul';

    // get primary key
    $primarykey = getPrimaryKey($schema,$tablename);
    // get column
    $column = getColumn($schema,$tablename);
    // get record data matkul 
    $matkul = getRecordMatkul($schema,$tablename);

    // response
    $response = [
        'schema' => $schema,
        'table' => $tablename,
        'primary_keys' => $primarykey,
        'struktur' => $column,
        'rows' => $matkul
    ];

    //enkripsi 
    $enkripsi = base64_encode(json_encode($response)); 

    // debug test
    // echo '<pre>';
    // print_r($response);
    // echo '</pre>';

    echo $enkripsi;
}

// get record matkul
function getRecordMatkul($schema,$tablename)
{
    global $conn;
    
    $sql = "SELECT * FROM ".$schema.".".$tablename." where last_update > last_sync";

    $result = pg_query($conn, $sql);
    while ($row = pg_fetch_assoc($result)) {
        $results[] = array_values($row);
    }
    
    return $results;
}

// get primary key
function getPrimaryKey($schema,$tablename)
{
    global $conn;

    $sql = "SELECT kcu.column_name
            FROM information_schema.table_constraints tco
            JOIN information_schema.key_column_usage kcu 
                ON kcu.constraint_name = tco.constraint_name
                AND kcu.constraint_schema = tco.constraint_schema
                AND kcu.constraint_name = tco.constraint_name
            WHERE tco.constraint_type = 'PRIMARY KEY' AND kcu.table_schema = '".$schema."' 
            and kcu.table_name = '".$tablename."'";

    $result = pg_query($conn, $sql);
    while ($row = pg_fetch_assoc($result)) {
        $results[] = $row['column_name'];
    }
    
    return $results;
}

// get column 
function getColumn($schema,$tablename)
{
    global $conn;

    $sql = "SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = '".$schema."'
                AND table_name   = '".$tablename."'";

    $result = pg_query($conn, $sql);
    while ($row = pg_fetch_assoc($result)) {
        $results[] = $row['column_name'];
    }
    
    return $results;
}

// run 
getDataMatkul();